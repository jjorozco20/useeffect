import React, { useEffect } from "react";

export default function NavBar({ title }) {
  useEffect(() => {
    console.log("2 case - useEffect");
  }, [title]);
  return <div>{title}</div>;
}
