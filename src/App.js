import React, { useEffect, useState } from "react";
import "./App.css";
import NavBar from "./NavBar";

function App() {
  const [title, setTitle] = useState("useEffect Example");
  const [dog, setDog] = useState();
  useEffect(() => {
    document.title = "useEffect - Example";
    console.log("1 case - useEffect");
  });

  useEffect(() => {
    let didCancel = false;
    console.log("3 case - useEffect");
    async function fetchDog() {
      if (!didCancel) {
        let response = await fetch("https://dog.ceo/api/breeds/image/random");
        const data = await response.json();
        setDog(data);
      }
    }
    fetchDog();
    return () => {
      didCancel = true;
    };
  }, []);

  return (
    <div className="App">
      <NavBar title={title} />
      {dog && <img src={dog.message} alt={dog.status} />}
      <button onClick={() => setTitle((preVale) => preVale + 1)}>
        Cause a re-render
      </button>
    </div>
  );
}

export default App;
